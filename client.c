#include <stdio.h>
#include <stdlib.h>

#include "shmem.h"

int
atoi_checked(const char * s)
{
    char * endptr;
    int num = strtol(s, &endptr, 0);
    if (*endptr != '\0') {
        fprintf(stderr, "%s: invalid character at %ld, expected integer\n", s, endptr - s);
        exit(EXIT_FAILURE);
    }
    return num;
}

int main(int argc, char ** argv)
{
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <index> <value>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int * shared_array = shared_block_init();
    puts("Successfully received int[10] array of shared memory");

    int idx = atoi_checked(argv[1]);
    int val = atoi_checked(argv[2]);

    printf("Replacing arr[%d] value: %d -> %d\n", idx, shared_array[idx], val);
    shared_array[idx] = val;

    shared_block_finish(shared_array);
    puts("Shared memory detached");
}
