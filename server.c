#include <stdio.h>

#include "shmem.h"

int
main()
{
    int * shared_array = shared_block_init();
    for (size_t i = 0; i < 10; i++) shared_array[i] = 0;
    puts("Initialized int[10] array of shared memory");

    while (getchar() != '\n');

    puts("Array state before destruction:");
    for (size_t i = 0; i < 10; i++) printf("\tarr[%zu] = %d\n", i, shared_array[i]);

    shared_block_finish(shared_array);
    puts("Shared memory destroyed successfully");

    return 0;
}
