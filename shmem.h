#pragma once

#ifdef SHMEM_SERVER
#  ifdef SHMEM_CLIENT
#    error Cannot define both SHMEM_SERVER and SHMEM_CLIENT at the same time
#  endif
#  define SHM_FLAGS IPC_CREAT | 0666
#else
#  define SHM_FLAGS 0666
#endif

#define PROCESS_ID 123

void *
shared_block_init();

void
shared_block_finish(void * mem);
