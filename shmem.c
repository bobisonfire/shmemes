#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "shmem.h"

void
perror_exit(const char * msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void
perrorf_exit(const char * format, ...)
{
    va_list ap;
    va_start(ap, format);

    char msg[256];
    vsnprintf(msg, 256, format, ap);

    va_end(ap);
    perror_exit(msg);
}

int
get_shmid()
{
    key_t shmkey = ftok(".", PROCESS_ID);
    if (shmkey < 0) perrorf_exit("ftok(\".\", 123)");

    int shmid = shmget(shmkey, 10 * sizeof(int), SHM_FLAGS);
    if (shmid < 0) perrorf_exit("shmget(%d, %zu, %o)", shmkey, 10 * sizeof(int), SHM_FLAGS);

    return shmid;
}

void *
shared_block_init()
{
    int shmid = get_shmid();
    void * mem = shmat(shmid, NULL, 0);
    if (mem == (void*) -1) perrorf_exit("shmat(%d, NULL, 0)", shmid);

    return mem;
}

void
shared_block_finish(void * mem)
{
    if (shmdt(mem) < 0) perrorf_exit("shmdt(%p)", mem);
#ifdef SHMEM_SERVER
    int shmid = get_shmid();
    if (shmctl(shmid, IPC_RMID, NULL)) perrorf_exit("shmctl(%d, IPC_RMID, NULL)", shmid);
#endif
}
