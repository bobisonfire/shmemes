all: server client

server: build/server/server.o build/server/shmem.o
client: build/client/client.o build/client/shmem.o

server client:
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@

build/server/%.o: %.c shmem.h
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -DSHMEM_SERVER -c -o $@ $<

build/client/%.o: %.c shmem.h
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -DSHMEM_CLIENT -c -o $@ $<

clean:
	rm -rf build server client
